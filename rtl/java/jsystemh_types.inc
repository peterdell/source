{

    This file is part of the Free Pascal Run time library.
    Copyright (c) 1999-2008 by the Free Pascal development team

    See the File COPYING.FPC, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}


{****************************************************************************
                             Needed switches
****************************************************************************}

{$I-,Q-,H-,R-,V-}

{$mode objfpc}
{$modeswitch advancedrecords}

{$IFDEF UNICODERTL}
{$modeswitch unicodestrings}
{ modeswitch typehelpers}
{$ENDIF}

{ At least 3.0.0 is required }
{$if FPC_FULLVERSION<30000}
  {$fatal You need at least FPC 3.0.0 to build this version of FPC}
{$endif}

{ Using inlining for small system functions/wrappers }
{$inline on}
{$ifndef DISABLE_SYSTEMINLINE}
  {$define SYSTEMINLINE}
{$endif}

{ don't use FPU registervariables on the i386 and i8086 }
{$if defined(CPUI386) or defined(CPUI8086)}
  {$maxfpuregisters 0}
{$endif CPUI386 or CPUI8086}

{ the assembler helpers need this}
{$ifdef CPUPOWERPC}
  {$goto+}
{$endif CPUPOWERPC}

{$ifdef CPUAVR}
  {$goto+}
{$endif CPUAVR}


{ needed for insert,delete,readln }
{$P+}
{ stack checking always disabled
  for system unit. This is because
  the startup code might not
  have been called yet when we
  get a stack error, this will
  cause big crashes
}
{$S-}

{$ifndef FPC_HAS_FEATURE_SUPPORT}
{$define FPC_HAS_FEATURE_HEAP}
{$define FPC_HAS_FEATURE_INITFINAL}
{$define FPC_HAS_FEATURE_RTTI}
{$define FPC_HAS_FEATURE_CLASSES}
{$define FPC_HAS_FEATURE_EXCEPTIONS}
{$define FPC_HAS_FEATURE_EXITCODE}
{$define FPC_HAS_FEATURE_ANSISTRINGS}
{$define FPC_HAS_FEATURE_WIDESTRINGS}
{$define FPC_HAS_FEATURE_TEXTIO}
{$define FPC_HAS_FEATURE_CONSOLEIO}
{$define FPC_HAS_FEATURE_FILEIO}
{$define FPC_HAS_FEATURE_RANDOM}
{$define FPC_HAS_FEATURE_VARIANTS}
{$define FPC_HAS_FEATURE_OBJECTS}
{$define FPC_HAS_FEATURE_DYNARRAYS}
{$define FPC_HAS_FEATURE_THREADING}
{$define FPC_HAS_FEATURE_COMMANDARGS}
{$define FPC_HAS_FEATURE_PROCESSES}
{$define FPC_HAS_FEATURE_STACKCHECK}
{$define FPC_HAS_FEATURE_DYNLIBS}
{$define FPC_HAS_FEATURE_OBJECTIVEC1}
{$define FPC_HAS_FEATURE_UNICODESTRINGS}
{$endif FPC_HAS_FEATURE_SUPPORT}

{ for now, the presence of unicode strings is just an approximation,
  USE_FILEREC_FULLNAME can be also enabled for other targets if
  they need file names longer than 255 chars }
{$if defined(FPC_HAS_FEATURE_UNICODESTRINGS)}
{$define USE_FILEREC_FULLNAME}
{$endif defined(FPC_HAS_FEATURE_UNICODESTRINGS)}

{****************************************************************************
                         Global Types and Constants
****************************************************************************}

{ some values which are used in RTL for TSystemCodePage type }
const
  CP_ACP     = 0;     // default to ANSI code page
  CP_OEMCP   = 1;     // default to OEM (console) code page
  CP_UTF16   = 1200;  // utf-16
  CP_UTF16BE = 1201;  // unicodeFFFE
  CP_UTF7    = 65000; // utf-7
  CP_UTF8    = 65001; // utf-8
  CP_ASCII   = 20127; // us-ascii
  CP_NONE    = $FFFF; // rawbytestring encoding

Type
  { The compiler has all integer types defined internally. Here
    we define only aliases }
  DWord    = LongWord;
  Cardinal = LongWord;
  Integer  = SmallInt;
  UInt64   = QWord;

  { moved here from psystem.pas
    Delphi allows chose of overloaded procedure depending
    on Real <-> Double, so use type here, see also tw7425.pp (FK) }
{$ifndef FPUNONE}
  Real = type Double;
{ Include generic version of float64 record }
{$i genmathh.inc}
{$endif}

{$ifdef CPUI386}
  {$define CPU32}

  {$define DEFAULT_EXTENDED}

  {$define SUPPORT_SINGLE}
  {$define SUPPORT_DOUBLE}
  {$define SUPPORT_EXTENDED}
  {$define SUPPORT_COMP}

  {$ifndef FPUNONE}
    ValReal = Extended;
  {$endif}

  FarPointer = NearFsPointer;
{$endif CPUI386}

{$ifdef CPUI8086}
  {$define CPU16}

  {$define DEFAULT_EXTENDED}

  {$define SUPPORT_SINGLE}
  {$define SUPPORT_DOUBLE}
  {$define SUPPORT_EXTENDED}
  {$define SUPPORT_COMP}

  {$ifndef FPUNONE}
    ValReal = Extended;
  {$endif}

  {$define MOVE_HAS_SIZEUINT_COUNT}
  {$define FILLCHAR_HAS_SIZEUINT_COUNT}

  {$if defined(FPC_MM_TINY)}
    {$define FPC_X86_CODE_NEAR}
    {$define FPC_X86_DATA_NEAR}
  {$elseif defined(FPC_MM_SMALL)}
    {$define FPC_X86_CODE_NEAR}
    {$define FPC_X86_DATA_NEAR}
  {$elseif defined(FPC_MM_MEDIUM)}
    {$define FPC_X86_CODE_FAR}
    {$define FPC_X86_DATA_NEAR}
  {$elseif defined(FPC_MM_COMPACT)}
    {$define FPC_X86_CODE_NEAR}
    {$define FPC_X86_DATA_FAR}
  {$elseif defined(FPC_MM_LARGE)}
    {$define FPC_X86_CODE_FAR}
    {$define FPC_X86_DATA_FAR}
  {$elseif defined(FPC_MM_HUGE)}
    {$define FPC_X86_CODE_FAR}
    {$define FPC_X86_DATA_HUGE}
  {$else}
    {$fatal No memory model defined}
  {$endif}
{$endif CPUI8086}

{$ifdef CPUX86_64}
{$ifdef FPC_HAS_TYPE_EXTENDED}
  { win64 doesn't support the legacy fpu }
  {$define DEFAULT_EXTENDED}
  {$define SUPPORT_EXTENDED}
  {$define SUPPORT_COMP}
  {$ifndef FPUNONE}
    ValReal = Extended;
  {$endif}
{$else FPC_HAS_TYPE_EXTENDED}
  {$define DEFAULT_DOUBLE}
  {$ifndef FPUNONE}
    ValReal = Double;
  {$endif}
{$endif FPC_HAS_TYPE_EXTENDED}

  {$define SUPPORT_SINGLE}
  {$define SUPPORT_DOUBLE}
{$endif CPUX86_64}

{$ifdef CPUM68K}
  {$define DEFAULT_DOUBLE}

  {$if defined(CPUCOLDFIRE) or defined(CPU68000)}
    {$define FPC_INCLUDE_SOFTWARE_MOD_DIV}
    {$define FPC_INCLUDE_SOFTWARE_MUL}
  {$endif}

  { m68k int64 shl/shr uses soft helper for non constant values }
  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    ValReal = Real;
  {$endif}
{$endif CPUM68K}

{$ifdef CPUPOWERPC}
  {$define DEFAULT_DOUBLE}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

    ValReal = Double;
  {$endif}
{$endif CPUPOWERPC}

{$ifdef CPUSPARC}
  {$define DEFAULT_DOUBLE}

  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    ValReal = Double;
  {$endif}
{$endif CPUSPARC}

{$ifdef CPUSPARC64}
  {$define DEFAULT_DOUBLE}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    ValReal = Double;
  {$endif}
{$endif CPUSPARC64}

{$if defined(CPUMIPS32) or defined(CPUMIPSEL32)}
  {$define DEFAULT_DOUBLE}

  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

    ValReal = Double;
  {$endif}
{$endif CPUMIPS32}

{$if defined(CPUMIPS64) or defined(CPUMIPS64EL)}
  {$define DEFAULT_DOUBLE}

  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}

  {$define SUPPORT_SINGLE}
  {$define SUPPORT_DOUBLE}

  {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

  ValReal = Double;
{$endif CPUMIPS64}


{$ifdef CPUARM}
  {$define DEFAULT_DOUBLE}

  {$define FPC_INCLUDE_SOFTWARE_MOD_DIV}
  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

    ValReal = Real;
  {$endif}
{$endif CPUARM}

{$ifdef CPUAVR}
  {$define DEFAULT_SINGLE}

  {$define FPC_INCLUDE_SOFTWARE_MOD_DIV}
  {$define FPC_INCLUDE_SOFTWARE_MUL}
  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

    ValReal = Real;
  {$endif}
{$endif CPUAVR}

{$ifdef CPUAARCH64}
  {$define DEFAULT_DOUBLE}

  {$define SUPPORT_SINGLE}
  {$define SUPPORT_DOUBLE}

  ValReal = Double;
{$endif CPUAARCH64}

{$ifdef CPURISCV32}
  {$define DEFAULT_DOUBLE}

  {$define FPC_INCLUDE_SOFTWARE_MOD_DIV}
  {$define FPC_INCLUDE_SOFTWARE_MUL}
  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}
  {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

    ValReal = Double;
  {$endif}
{$endif CPURISCV32}

{$ifdef CPURISCV64}
  {$define DEFAULT_DOUBLE}

  {$define FPC_INCLUDE_SOFTWARE_MOD_DIV}
  {$define FPC_INCLUDE_SOFTWARE_MUL}
  {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

    ValReal = Double;
  {$endif}
{$endif CPURISCV64}

{$ifdef CPUXTENSA}
  {$define DEFAULT_DOUBLE}

  {$define FPC_INCLUDE_SOFTWARE_MOD_DIV}
  {$define FPC_INCLUDE_SOFTWARE_MUL}
  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    ValReal = Double;
  {$endif}
{$endif CPUXTENSA}

{$ifdef CPUZ80}
  {$define DEFAULT_SINGLE}

  {$define FPC_INCLUDE_SOFTWARE_MOD_DIV}
  {$define FPC_INCLUDE_SOFTWARE_MUL}
  {$define FPC_INCLUDE_SOFTWARE_SHIFT_INT64}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    {$define FPC_INCLUDE_SOFTWARE_INT64_TO_DOUBLE}

    ValReal = Real;
  {$endif}

  FarPointer = Pointer;
{$endif CPUZ80}

{$ifdef CPUWASM32}
  {$define DEFAULT_DOUBLE}

  {$ifndef FPUNONE}
    {$define SUPPORT_SINGLE}
    {$define SUPPORT_DOUBLE}

    ValReal = Double;
  {$endif}

  FarPointer = Pointer;
{$endif CPUWASM32}

{$ifdef CPULOONGARCH64}
  {$define DEFAULT_DOUBLE}

  {$define SUPPORT_SINGLE}
  {$define SUPPORT_DOUBLE}

  ValReal = Double;
{$endif CPULOONGARCH64}

{$ifdef CPUJVM}
  {$define DEFAULT_DOUBLE}

  {$define SUPPORT_SINGLE}
  {$define SUPPORT_DOUBLE}

  ValReal = Double;

  { Map comp to int64, but this doesn't mean we compile the comp support in! }
  Comp = Int64;
{$endif CPUJVM}

{ By default enable a simple implementation of Random for 8/16 bit CPUs }
{$if (defined(CPU16) or defined(CPU8)) and not defined(FPC_NO_SIMPLE_RANDOM)}
  {$define FPC_USE_SIMPLE_RANDOM}
{$endif}


{$if not declared(FarPointer)}
  FarPointer = Pointer;
{$endif}


{$ifdef CPU64}
  SizeInt = Int64;
  SizeUInt = QWord;
  PtrInt = Int64;
  PtrUInt = QWord;
  ValSInt = int64;
  ValUInt = qword;
  CodePointer = Pointer;
  CodePtrInt = PtrInt;
  CodePtrUInt = PtrUInt;
  TExitCode = Longint;
{$endif CPU64}

{$ifdef CPU32}
  SizeInt = Longint;
  SizeUInt = DWord;
  PtrInt = Longint;
  PtrUInt = DWord;
  ValSInt = Longint;
  ValUInt = Cardinal;
  CodePointer = Pointer;
  CodePtrInt = PtrInt;
  CodePtrUInt = PtrUInt;
  TExitCode = Longint;
{$endif CPU32}

{$ifdef CPU16}
  SizeInt = Integer;
  SizeUInt = Word;
  {$if defined(FPC_X86_DATA_FAR) or defined(FPC_X86_DATA_HUGE)}
    PtrInt = Longint;
    PtrUInt = DWord;
  {$else}
    PtrInt = Integer;
    PtrUInt = Word;
  {$endif}
  {$if defined(FPC_X86_CODE_FAR)}
    CodePointer = FarPointer;
    CodePtrInt = Longint;
    CodePtrUInt = DWord;
  {$elseif defined(FPC_MM_TINY)}
    CodePointer = NearPointer;
    CodePtrInt = Integer;
    CodePtrUInt = Word;
  {$elseif defined(FPC_X86_CODE_NEAR)}
    CodePointer = NearCsPointer;
    CodePtrInt = Integer;
    CodePtrUInt = Word;
  {$else}
    CodePointer = Pointer;
    CodePtrInt = PtrInt;
    CodePtrUInt = PtrUInt;
  {$endif}
  ValSInt = Integer;
  ValUInt = Word;
  { this is TP compatible }
  TExitCode = Word;
{$endif CPU16}

{$if defined(VER3_0)}
{$if defined(CPU16)}
{$define CPUINT16}
{$elseif defined(CPU32)}
{$define CPUINT32}
{$elseif defined(CPU64)}
{$define CPUINT64}
{$endif defined(CPU64)}
{$endif defined(VER3_0)}

{$if defined(CPUINT8)}
  ALUSInt = ShortInt;
  ALUUInt = Byte;
{$elseif defined(CPUINT16)}
  ALUSInt = SmallInt;
  ALUUInt = Word;
{$elseif defined(CPUINT32)}
  ALUSInt = Longint;
  ALUUInt = DWord;
{$elseif defined(CPUINT64)}
  ALUSInt = Int64;
  ALUUInt = QWord;
{$endif defined(CPUINT64)}

  { NativeInt and NativeUInt are Delphi compatibility types. Even though Delphi
    has IntPtr and UIntPtr, the Delphi documentation for NativeInt states that
    'The size of NativeInt is equivalent to the size of the pointer on the
    current platform'. Because of the misleading names, these types shouldn't be
    used in the FPC RTL. Note that on i8086 their size changes between 16-bit
    and 32-bit according to the memory model, so they're not really a 'native
    int' type there at all. }
  NativeInt  = Type PtrInt;
  NativeUInt = Type PtrUInt;

  Int8    = ShortInt;
  Int16   = SmallInt;
  Int32   = Longint;
  IntPtr  = PtrInt;
  UInt8   = Byte;
  UInt16  = Word;
  UInt32  = Cardinal;
  UIntPtr = PtrUInt;

{$if not declared(Comp)}
  { map comp to int64, but this doesn't mean we compile the comp support in! }
  Comp = type Int64;
{$endif}

{ Zero - terminated strings }

{$IF DECLARED(AnsiChar)}
// Compiler defines AnsiChar and WideChar, not AnsiChar
{$IFDEF UNICODERTL}
  Char = WideChar;
{$ElSE}
  Char = AnsiChar;
{$ENDIF}

{$ELSE}
  // Compiler defines Char, we make AnsiChar an alias
  AnsiChar = char;
{$ENDIF}

  PChar               = ^Char;
  PPChar              = ^PChar;
  PPPChar             = ^PPChar;

  TAnsiChar           = AnsiChar;
  PAnsiChar           = ^AnsiChar;
  PPAnsiChar          = ^PAnsiChar;
  PPPAnsiChar         = ^PPAnsiChar;

  UTF8Char = AnsiChar;
  PUTF8Char = PAnsiChar;

  UCS4Char            = 0..$10ffff;
  PUCS4Char           = ^UCS4Char;
{$ifdef CPU16}
  TUCS4CharArray      = array[0..32767 div sizeof(UCS4Char)-1] of UCS4Char;
{$else CPU16}
  TUCS4CharArray      = array[0..$effffff] of UCS4Char;
{$endif CPU16}
  PUCS4CharArray      = ^TUCS4CharArray;
{$ifdef FPC_HAS_FEATURE_DYNARRAYS}
  UCS4String          = array of UCS4Char;
{$endif}

{$ifdef FPC_HAS_CPSTRING}
  UTF8String          = type AnsiString(CP_UTF8);
{$else FPC_HAS_CPSTRING}
  UTF8String          = type ansistring;
{$endif FPC_HAS_CPSTRING}
  PUTF8String         = ^UTF8String;

{$ifdef FPC_HAS_CPSTRING}
  RawByteString       = type AnsiString(CP_NONE);
{$else FPC_HAS_CPSTRING}
  RawByteString       = ansistring;
{$endif FPC_HAS_CPSTRING}

  HRESULT             = type Longint;
{$ifndef FPUNONE}
  TDateTime           = type Double;
  TDate               = type TDateTime;
  TTime               = type TDateTime;
{$endif}
  TError               = type Longint;

{$ifndef FPUNONE}
  PSingle             = ^Single;
  PDouble             = ^Double;
  PExtended           = ^Extended;

  PPDouble            = ^PDouble;
{$endif}
  PCurrency           = ^Currency;
{$if declared(Comp)}
  PComp               = ^Comp;
{$endif declared(Comp)}

  PSmallInt           = ^Smallint;
  PShortInt           = ^Shortint;
  PInteger            = ^Integer;
  PByte               = ^Byte;
  PWord               = ^word;
  PDWord              = ^DWord;
  PLongWord           = ^LongWord;
  PLongint            = ^Longint;
  PCardinal           = ^Cardinal;
  PQWord              = ^QWord;
  PInt64              = ^Int64;
  PUInt64             = ^UInt64;
  PPtrInt             = ^PtrInt;
  PPtrUInt            = ^PtrUInt;
  PSizeInt            = ^SizeInt;
  PSizeUInt           = ^SizeUInt;

  PPByte              = ^PByte;
  PPLongint           = ^PLongint;

  PPointer            = ^Pointer;
  PPPointer           = ^PPointer;

  PCodePointer        = ^CodePointer;
  PPCodePointer       = ^PCodePointer;

  PBoolean            = ^Boolean;

{$IFNDEF VER3_0}
  PBoolean8           = ^Boolean8;
{$ENDIF VER3_0}
  PBoolean16          = ^Boolean16;
  PBoolean32          = ^Boolean32;
  PBoolean64          = ^Boolean64;

  PByteBool           = ^ByteBool;
  PWordBool           = ^WordBool;
  PLongBool           = ^LongBool;
  PQWordBool          = ^QWordBool;

  PNativeInt 	      = ^NativeInt;
  PNativeUInt	      = ^NativeUint;
  pInt8   	      = PShortInt;
  pInt16  	      = PSmallint;
  pInt32  	      = PLongint;
  PIntPtr 	      = PPtrInt;
  pUInt8  	      = PByte;
  pUInt16 	      = PWord;
  pUInt32 	      = PDWord;
  PUintPtr	      = PPtrUInt;

  PShortString        = ^ShortString;
  PAnsiString         = ^AnsiString;
  PRawByteString      = ^RawByteString;

{$ifndef FPUNONE}
  PDate               = ^TDateTime;
  PDateTime           = ^TDateTime;
{$endif}
  PError              = ^TError;
{$ifdef FPC_HAS_FEATURE_VARIANTS}
  PVariant            = ^Variant;
  POleVariant         = ^OleVariant;
{$endif FPC_HAS_FEATURE_VARIANTS}

  PWideChar           = ^WideChar;
  PPWideChar          = ^PWideChar;
  PPPWideChar         = ^PPWideChar;
  WChar               = Widechar;
  UCS2Char            = WideChar;
  PUCS2Char           = PWideChar;
  PWideString         = ^WideString;

  UnicodeChar         = WideChar;
  PUnicodeChar        = ^UnicodeChar;
  PUnicodeString      = ^UnicodeString;

  PMarshaledString    = ^PWideChar;
  PMarshaledAString   = ^PAnsiChar;

  MarshaledString     = PWideChar;
  MarshaledAString    = PAnsiChar;

  TSystemCodePage     = Word;

  TFileTextRecChar    = {$if defined(FPC_ANSI_TEXTFILEREC) or not(defined(FPC_HAS_FEATURE_WIDESTRINGS))}AnsiChar{$else}UnicodeChar{$endif};
  PFileTextRecChar    = ^TFileTextRecChar;

  TTextLineBreakStyle = (tlbsLF,tlbsCRLF,tlbsCR);

{ opaque data type and related opaque pointer }
  TOpaqueData = record end;
  POpaqueData = ^TOpaqueData;
  OpaquePointer = type POpaqueData;

{ procedure type }
  TProcedure  = Procedure;

{ platform-dependent types }
{$i sysosh.inc}

{ platform-dependent defines }
{$i rtldefs.inc}
(*
{*****************************************************************************
                   TextRec/FileRec exported to allow compiler to take size
*****************************************************************************}

{$ifdef FPC_HAS_FEATURE_FILEIO}
{$i filerec.inc}
{$endif FPC_HAS_FEATURE_FILEIO}

{$i textrec.inc}


type
  { Needed for fpc_get_output }
  PText               = ^Text;

  TEntryInformation = record
    InitFinalTable : Pointer;
    ThreadvarTablesTable : Pointer;
    ResourceStringTables : Pointer;
    ResStrInitTables : Pointer;
    ResLocation : Pointer;
    PascalMain : Procedure;
    valgrind_used : boolean;
    {$ifdef HAS_ENTRYINFORMATION_OS}
    OS : TEntryInformationOS;
    {$endif HAS_ENTRYINFORMATION_OS}
  end;
*)

const
{ Maximum value of the biggest signed and unsigned integer type available}
  MaxSIntValue = High(ValSInt);
  MaxUIntValue = High(ValUInt);

{ max. values for longint and int}
  maxLongint  = $7fffffff;
  maxSmallint = 32767;

  maxint   = maxsmallint;

type
{$ifdef CPU16}
  IntegerArray  = array[0..maxSmallint div sizeof(Integer)-1] of Integer;
{$else CPU16}
  IntegerArray  = array[0..$effffff] of Integer;
{$endif CPU16}
  PIntegerArray = ^IntegerArray;
{$ifdef CPU16}
  PointerArray = array [0..32767 div sizeof(Pointer)-1] of Pointer;
{$else CPU16}
  PointerArray = array [0..512*1024*1024-2] of Pointer;
{$endif CPU16}
  PPointerArray = ^PointerArray;

  TBoundArray = array of SizeInt;

{$ifdef CPU16}
  TPCharArray = packed array[0..(MaxSmallint div SizeOf(PAnsiChar))-1] of PAnsiChar;
{$else CPU16}
  TPCharArray = packed array[0..(MaxLongint div SizeOf(PAnsiChar))-1] of PAnsiChar;
{$endif CPU16}
  PPCharArray = ^TPCharArray;

(* CtrlBreak set to true signalizes Ctrl-Break signal, otherwise Ctrl-C. *)
(* Return value of true means that the signal has been processed, false  *)
(* means that default handling should be used. *)
  TCtrlBreakHandler = function (CtrlBreak: boolean): boolean;

{ Numbers for routines that have compiler magic }
{$I innr.inc}

{ CPU specific stuff }
{$i cpuh.inc}

const
{ max level in dumping on error }
  Max_Frame_Dump : Word = 8;

{ Exit Procedure handling consts and types  }
  ExitProc : codepointer = nil;
  Erroraddr: codepointer = nil;
  Errorcode: Word    = 0;

{ file input modes }
  fmClosed = $D7B0;
  fmInput  = $D7B1;
  fmOutput = $D7B2;
  fmInOut  = $D7B3;
  fmAppend = $D7B4;
  Filemode : byte = 2;
(* Value should be changed during system initialization as appropriate. *)

  { assume that this program will not spawn other threads, when the
    first thread is started the following constants need to be filled }
  IsMultiThread : longbool = FALSE;
  { set to true, if a threading helper is used before a thread
    manager has been installed }
  ThreadingAlreadyUsed : boolean = FALSE;
  { Indicates if there was an error }
  StackError : boolean = FALSE;
  InitProc : CodePointer = nil;
  { compatibility }
  ModuleIsLib : Boolean = FALSE;
  ModuleIsPackage : Boolean = FALSE;
  ModuleIsCpp : Boolean = FALSE;

var
  ExitCode    : TExitCode; public name 'operatingsystem_result';
  RandSeed    : Cardinal;
  { Delphi compatibility }

{$ifdef FPC_HAS_FEATURE_DYNLIBS}
  IsLibrary : boolean = false; public name 'operatingsystem_islibrary';
{$else FPC_HAS_FEATURE_DYNLIBS}
const
  IsLibrary = false;
var
{$endif FPC_HAS_FEATURE_DYNLIBS}
  IsConsole : boolean = false; public name 'operatingsystem_isconsole';
  NoErrMsg: Boolean platform = False; // For Delphi compatibility, not used in FPC.
  FirstDotAtFileNameStartIsExtension : Boolean = False;

  DefaultSystemCodePage,
  DefaultUnicodeCodePage,
  { the code page to use when sending paths/file names to OS file system API
    calls using single byte strings, and to interpret the results gotten back
    from such API calls }
  DefaultFileSystemCodePage,
  { the code page to use to return file names from single byte file system calls
    in the RTL that return ansistrings (by default, same as a above) }
  DefaultRTLFileSystemCodePage,
  UTF8CompareLocale : TSystemCodePage;


{$ifndef HAS_CMDLINE}
{Value should be changed during system initialization as appropriate.}
var CmdLine:PAnsiChar=nil; // MVC: TODO ?
{$endif}

(*
{$ifdef FPC_HAS_FEATURE_THREADING}
ThreadVar
{$else FPC_HAS_FEATURE_THREADING}
Var
{$endif FPC_HAS_FEATURE_THREADING}
  ThreadID    : TThreadID;
  { Standard In- and Output }
{$ifndef FPC_STDERR_IS_ALIAS_FOR_STDOUT}
  ErrOutput,
{$endif FPC_STDERR_IS_ALIAS_FOR_STDOUT}
  Output,
  Input       : Text;
{$ifdef FPC_STDERR_IS_ALIAS_FOR_STDOUT}
  ErrOutput   : Text Absolute Output;
{$endif FPC_STDERR_IS_ALIAS_FOR_STDOUT}
{$ifndef FPC_STDOUT_TRUE_ALIAS}
  StdOut,
  StdErr      : Text;
{$else FPC_STDOUT_TRUE_ALIAS}
  { Optionally, make StdOut and StdErr a true alias of Output
    and ErrOutput. This is benefical on small systems where the
    data segment size is limited, as it lessens the RTL's data
    size by a bit more than 1K. }
  StdOut      : Text Absolute Output;
  StdErr      : Text Absolute ErrOutput;
{$endif FPC_STDOUT_TRUE_ALIAS}
  InOutRes    : Word;
  { Stack checking }
  StackBottom : Pointer;
  StackLength : SizeUInt;

Var
  WriteErrorsToStdErr : Boolean = True;

function StackTop: Pointer;
*)


